import time
import pandas as pd
from bs4 import BeautifulSoup
from selenium import webdriver
import json

# @ 28.2.2024 revision 29.2.2024
def scrape_zhihu_question(url):

    
    driver = webdriver.Chrome()   # Chrome seems to be better at the rate limit page
    driver.get(url)


    def scroll_to_bottom(driver):
        """Simulates scrolling down the page until no more new content is loaded."""
        last_height = driver.execute_script("return document.body.scrollHeight")
        while True:
            time.sleep(2)
            # Scroll down to bottom
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            # Wait to load page
            time.sleep(5)  # Adjust this time delay as needed
            # Calculate new scroll height and compare with last scroll height
            new_height = driver.execute_script("return document.body.scrollHeight")
            if new_height == last_height:
                break
            last_height = new_height


    # Simulate scrolling down the page
    scroll_to_bottom(driver)

    # Parse the HTML content of the page using BeautifulSoup
    soup = BeautifulSoup(driver.page_source, "html.parser")

    # Close the WebDriver
    driver.quit()

    # Extract the URL of the question
    question_url_meta = soup.find('meta', itemprop='url')
    question_url = question_url_meta['content'] if question_url_meta else 'Unknown'
    
    # Extract the QID from the URL
    qid_base = url.split('/')[-1]
    
    question_header = soup.find('h1', class_='QuestionHeader-title')
    question = question_header.text.strip() if question_header else 'Question not found'
    
    answers = soup.find_all('div', {'class': 'ContentItem AnswerItem'})

    questions_list = []
    question_urls_list = []
    qids_list = []
    authors_list = []
    answers_list = []
    user_links_list = []
    timestamps_list = []
    #comments_list = []
    image_urls_list = []
    upvotes_list = []
    
    # Process each answer
    for index, answer in enumerate(answers, start=1):
        
        # Extract author name from data-zop attribute
        data_zop = answer.get('data-zop')
        author = 'Unknown'
        if data_zop:
            try:
                data_zop_json = json.loads(data_zop)
                author = data_zop_json.get('authorName', 'Unknown')
            except json.JSONDecodeError:
                pass  # Handle potential JSON parsing errors gracefully

        # Extract answer text
        answer_text = answer.get_text().strip()

        # Extract user link
        user_link_tag = answer.find('a', class_='UserLink-link')
        user_link = 'https:' + user_link_tag['href'] if user_link_tag else 'No link'

        # Extract timestamp information and remove '发布于'
        timestamp_span = answer.find('span', {'data-tooltip': True, 'aria-label': True})
        timestamp = timestamp_span['aria-label'].replace('发布于 ', '') if timestamp_span else 'Unknown' # Corregir, no obtenemos timestamp en algunos posts

        # Extract upvotes using the meta tag
        upvote_meta = answer.find('meta', itemprop='upvoteCount')
        upvotes = upvote_meta['content'] if upvote_meta else '0'
       
        # Extract comments // Div classes have different hashes for the moment is difficult to scrape
        #comment_meta = answer.find('meta', itemprop='commentCount')
        #comments = comment_meta['content'] if comment_meta else '0'
        
        # Extract image URL or set to None if no image is found
        image_tag = answer.find('img', class_='origin_image zh-lightbox-thumb lazy')
        image_url = image_tag['data-original'] if image_tag else None
        
        
        # Append data to lists
        questions_list.append(question)
        question_urls_list.append(question_url)
        qids_list.append(f"{qid_base}a{index}")  
        authors_list.append(author)
        answers_list.append(answer_text)
        user_links_list.append(user_link)
        timestamps_list.append(timestamp)
        upvotes_list.append(upvotes)
        #comments_list.append(comments)
        image_urls_list.append(image_url)
    
    # Create a DataFrame from the lists
    df = pd.DataFrame({
        'Question': questions_list,
        'URL': question_urls_list,
        'Author': authors_list,
        'Author_url': user_links_list,
        'Content': answers_list,
        'PID': qids_list,
        'Image_URL': image_urls_list,
        'Date': timestamps_list,
        'Approval': upvotes_list,
        #'Comments': comments
        })

    return df

# Load search data -----------------------------------------------------------------------
clean_2_data = pd.read_csv('name_search_nodup.csv')

# Filter URLs that contain 'question'
questions_urls = clean_2_data[clean_2_data['url'].str.contains('question')]['url'].tolist()

# Scrape data from the filtered URLs and concatenate results
all_question_data = pd.DataFrame()
for question_url in questions_urls[:10]:                         # Scrape 10 at a time to avoid blocking
    print(question_url)
    time.sleep(1)
    
    question_data = scrape_zhihu_question(question_url)
    all_question_data = pd.concat([all_question_data, question_data])

# Save the combined DataFrame to a CSV file
all_question_data.to_csv("name_answer_dinamic.csv", index=False, encoding="utf-8")

print("Scraped data saved to csv ...")
