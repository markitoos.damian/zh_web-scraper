import pandas as pd

# Load the CSV file into a DataFrame
df = pd.read_csv('column_name.csv')

# Check for duplicates based on the 'url' column
duplicates = df[df.duplicated(subset=['Content'], keep=False)]

if duplicates.empty:
    print("No duplicates found based on the 'Content' column.")
else:
    print("Duplicates based on the 'Content' column:")
    print(duplicates)


# Remove duplicates based on the 'url' column, keeping the first occurrence
search_no_duplicates = df.drop_duplicates(subset=['Content'], keep='first')

# Save the DataFrame without duplicates to a new CSV file
search_no_duplicates.to_csv('./column_no_duplicates.csv', index=False)    
