import requests
from bs4 import BeautifulSoup
import pandas as pd
import re
import time

# @ Revision 4.3.2024 
def scrape_column(urls, max_retries=3, retry_delay=5, loop_delay=2):
    """
    Scrapes data from a list of Zhihu column URLs, handling potential errors.

    Args:
        urls (list): A list of Zhihu column URLs.
        max_retries (int, optional): The maximum number of retries for failed requests.
                                    Defaults to 3.
        retry_delay (int, optional): The delay in seconds between retries.
                                    Defaults to 5.
        loop_delay (int, optional): The delay in seconds between scraping each URL.
                                    Defaults to 2.

    Returns:
        pandas.DataFrame: A DataFrame containing the scraped data, or an empty DataFrame
                         if all requests fail after retries.
    """

    data = []
    for url in urls:
        print(url)
        retries = 0
        while retries < max_retries:
            try:
                response = requests.get(url)
                response.raise_for_status()  # Raise an exception for non-200 status codes

                html_content = response.content
                soup = BeautifulSoup(html_content, 'html.parser')

                # 1. Find the title of the page
                title = soup.find('h1', class_='Post-Title')
                title = title.text.strip() if title else "No title found"

                # 2. Author
                author_info = soup.find('div', class_='AuthorInfo')
                author = author_info.text.strip() if author_info else "No author found"
                author_url_meta = author_info.find('meta', itemprop='url') if author_info else None
                author_url = author_url_meta['content'] if author_url_meta else 'URL not available'

                # 3. Find the main content of the page
                content = soup.find('div', class_='RichText')
                content = content.text.strip() if content else "No content found"

                # 4. Find the publication time
                time = soup.find('div', class_='ContentItem-time').text.strip()
                date = re.search(r'\d{4}-\d{2}-\d{2} \d{2}:\d{2}', time).group() if time else "No date found"
                
                # 5. Find the first picture in the article (if exists)
                picture = soup.find('div', class_='RichText').find('img')
                picture_url = picture['src'] if picture else "No picture found"

                # 6. Extract the article URL from the meta tag
                article_url = soup.find('meta', property='og:url')['content']

                # 7. Extract PID from the URL
                pid = url.split('/')[-1][:9]

                 # Extract approval (likes) from the button
                approval_button = soup.find('button', {'aria-label': lambda x: x and x.startswith('赞同')})
                if approval_button:
                    approval_text = approval_button['aria-label']
                    approval = re.search(r'\d+', approval_text).group() if re.search(r'\d+', approval_text) else "0"
                else:
                    approval = "Approval data not found"
                
                # Append the data to the list
                data.append({
                    'Title': title,
                    'URL': article_url,
                    'Author': author,
                    'Author_url': author_url,
                    'Content': content,
                    'PID': pid,
                    'Image_URL': picture_url,
                    'Date': date,
                    'Approval': approval
                })

                break  # Exit the loop after successful scraping

            except requests.exceptions.RequestException as e:
                print(f"Error encountered while scraping {url}: {e}")
                retries += 1
                if retries < max_retries:
                    print(f"Retrying request for {url} in {retry_delay} seconds...")
                    time.sleep(retry_delay)
                else:
                    print(f"Failed to scrape {url} after {max_retries} retries.")

            # Add a delay between scraping each URL
            time.sleep(loop_delay)

    # Convert the list of dictionaries to a DataFrame
    df = pd.DataFrame(data)

    return df

#Load search data 
clean_2_data = pd.read_csv('name_search_nodup.csv')

# Filter URLs that contain 'zhuanlan'
zhuanlan_urls = clean_2_data[clean_2_data['url'].str.contains('zhuanlan')]['url'].tolist()

# Scrape data from the filtered URLs
scraped_data = scrape_column(zhuanlan_urls)
scraped_data.to_csv("column_name.csv", index=False, encoding="utf-8")
