import pandas as pd
import time
from autoscraper import AutoScraper


def load_scraper_model(model_path):
  """Loads a previously saved AutoScraper model.

  Args:
      model_path (str): The path to the saved model file.

  Returns:
      autoscraper.AutoScraper: The loaded AutoScraper model.
  """
  scraper = AutoScraper()
  scraper.load(model_path)
  return scraper


def scrape_data(scraper, urls, delay=5):
  """Scrapes data from a list of URLs using a provided AutoScraper model.

  Args:
      scraper (autoscraper.AutoScraper): The loaded AutoScraper model.
      urls (list): A list of URLs to scrape data from.
      delay (int, optional): The number of seconds to wait between requests. Defaults to 5.

  Returns:
      pd.DataFrame: A DataFrame containing the scraped data.
  """
  df_result = pd.DataFrame()
  for url in urls:
    print(url)
    result = scraper.get_result_similar(url, grouped=True)
    df_temp = pd.DataFrame(result)
    df_result = df_result._append(df_temp, ignore_index=True)
    time.sleep(2)  # Add delay to avoid blocking

  return df_result


def clean_and_save_data(df, output_file):
  """Cleans and saves the scraped data to a CSV file.

  Args:
      df (pd.DataFrame): The DataFrame containing the scraped data.
      output_file (str): The path to the output CSV file.
  """
  # Extract date from specific column
  df["date"] = df["rule_45iv"].str.extract(r"(\d{4}-\d{1,2}-\d{1,2})")

  # Rename columns for clarity
  df = df.drop(columns=["rule_45iv"]).rename(columns={"rule_6wio": "content", "rule_6sgb": "url"})

  # Save to CSV
  df.to_csv(output_file, index=False, encoding="utf-8")


def check_duplicate_urls(csv_file):
  """Checks for and removes duplicate URLs from a CSV file, saving a clean version.

  Args:
      csv_file (str): The path to the CSV file to check.
  """
  df = pd.read_csv(csv_file)
  df_clean = df.drop_duplicates(subset="url")

  num_duplicates = len(df) - len(df_clean)
  if num_duplicates > 0:
    print(f"Removed {num_duplicates} duplicate URLs from '{csv_file}'.")
    df_clean.to_csv("clean_temu_search.csv", index=False, encoding="utf-8")
  else:
    print(f"No duplicate URLs found in '{csv_file}'.")


def main():
  # Load the scraper model (replace with your actual model path)
  scraper = load_scraper_model("my_scraper_model")

  # Define list of URLs to scrape (replace with your actual URLs)
  urls = [
   'Add up to 2 urls, then rotate ips',
    ]

  # Scrape data
  df = scrape_data(scraper, urls)

  # Clean and save data
  clean_and_save_data(df, "name_search.csv")         # Enter name of csv with url's

  # Check for duplicate URLs
  check_duplicate_urls("name_search.csv")


if __name__ == "__main__":
  main()
